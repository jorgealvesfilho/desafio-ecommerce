﻿using System.ComponentModel.DataAnnotations;

namespace Desafio.Ecommerce.Modelos
{
    public class CartaoCredito
    {
        [Required]
        public string Bandeira { get; set; }

        [Required]
        [CreditCard]
        public string Numero { get; set; }

        [Required]
        public string NomeTitular { get; set; }

        [Required]
        [MaxLength(5)]
        public string DataValidade { get; set; }

        [Required]
        [MaxLength(3)]
        public string CodigoSeguranca { get; set; }
    }

    public class CartaoCreditoProtegido
    {
        public string Bandeira { get; set; }
        public string Numero { get; set; }
        public string NomeTitular { get; set; }
        public string DataValidade { get; set; }
        public string CodigoSeguranca { get; set; }
    }

    public class CartaoCreditoSaida
    {
        public string Bandeira { get; set; }
        public string Numero { get; set; }
        public string NomeTitular { get; set; }
        public string DataValidade { get; set; }
    }
}