﻿using MongoDB.Bson;
using System.ComponentModel.DataAnnotations;

namespace Desafio.Ecommerce.Modelos
{
    public class TaxaAdquirente
    {
        public ObjectId Id { get; set; }

        [Required]
        public string Adquirente { get; set; }

        [Required]
        public string Bandeira { get; set; }

        [Required]
        public decimal Valor { get; set; }
    }
}