﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Desafio.Ecommerce.Modelos
{
    public class Registro
    {
        [Required]
        public string Usuario { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Senha { get; set; }

        [DataType(DataType.Password)]
        [Compare("Senha", ErrorMessage = "Confirmação de senha diferente da senha.")]
        public string ConfirmacaoSenha { get; set; }

        public List<string> Perfis { get; set; }
    }
}