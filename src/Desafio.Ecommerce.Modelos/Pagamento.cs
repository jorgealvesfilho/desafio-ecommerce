﻿using MongoDB.Bson;
using System.ComponentModel.DataAnnotations;

namespace Desafio.Ecommerce.Modelos
{
    public class Pagamento
    {
        public ObjectId Id { get; set; }

        [Required]
        public string Adquirente { get; set; }

        [Required]
        public decimal Valor { get; set; }

        public decimal TaxaAdquirente { get; set; }
        public decimal ValorAdquirente { get; set; }
        public decimal ValorLojista { get; set; }

        [Required]
        public CartaoCredito CartaoCredito { get; set; }
    }

    public class PagamentoProtegido
    {
        public ObjectId Id { get; set; }
        public string Adquirente { get; set; }
        public decimal Valor { get; set; }
        public decimal TaxaAdquirente { get; set; }
        public decimal ValorAdquirente { get; set; }
        public decimal ValorLojista { get; set; }
        public CartaoCreditoProtegido CartaoCredito { get; set; }
    }

    public class PagamentoSaida
    {
        public ObjectId Id { get; set; }
        public string Adquirente { get; set; }
        public decimal Valor { get; set; }
        public decimal TaxaAdquirente { get; set; }
        public decimal ValorAdquirente { get; set; }
        public decimal ValorLojista { get; set; }
        public CartaoCreditoSaida CartaoCredito { get; set; }
    }
}