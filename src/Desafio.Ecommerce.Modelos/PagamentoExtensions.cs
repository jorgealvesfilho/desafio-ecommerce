﻿namespace Desafio.Ecommerce.Modelos
{
    public static class PagamentoExtensions
    {
        public static PagamentoSaida ToSaida(this PagamentoProtegido pagamento)
        {
            return new PagamentoSaida
            {
                Id = pagamento.Id,
                Adquirente = pagamento.Adquirente,
                Valor = pagamento.Valor,
                TaxaAdquirente = pagamento.TaxaAdquirente,
                ValorAdquirente = pagamento.ValorAdquirente,
                ValorLojista = pagamento.ValorLojista,
                CartaoCredito = pagamento.CartaoCredito.ToSaida()
            };
        }

        public static PagamentoProtegido ToPagamentoProtegido(this Pagamento pagamento)
        {
            return new PagamentoProtegido
            {
                Id = pagamento.Id,
                Adquirente = pagamento.Adquirente,
                Valor = pagamento.Valor,
                TaxaAdquirente = pagamento.TaxaAdquirente,
                ValorAdquirente = pagamento.ValorAdquirente,
                ValorLojista = pagamento.ValorLojista,
                CartaoCredito = pagamento.CartaoCredito.ToCartaoCreditoProtegido()
            };
        }
    }
}