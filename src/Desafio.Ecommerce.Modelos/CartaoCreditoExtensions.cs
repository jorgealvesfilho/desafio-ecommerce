﻿using Desafio.Ecommerce.Seguranca;

namespace Desafio.Ecommerce.Modelos
{
    public static class CartaoCreditoExtensions
    {
        public static CartaoCreditoSaida ToSaida(this CartaoCreditoProtegido cartaoCredito)
        {
            CartaoCreditoSaida cartaoCreditoSaida = new CartaoCreditoSaida();
            cartaoCreditoSaida.Bandeira = cartaoCredito.Bandeira;
            cartaoCreditoSaida.NomeTitular = CriptografiaHelper.Decriptar(cartaoCredito.NomeTitular);
            cartaoCreditoSaida.DataValidade = CriptografiaHelper.Decriptar(cartaoCredito.DataValidade);

            string numero = CriptografiaHelper.Decriptar(cartaoCredito.Numero);
            cartaoCreditoSaida.Numero = new string('X', numero.Length - 4) + numero.Substring(numero.Length - 4);

            return cartaoCreditoSaida;
        }

        public static CartaoCreditoProtegido ToCartaoCreditoProtegido(this CartaoCredito cartaoCredito)
        {
            return new CartaoCreditoProtegido
            {
                Bandeira = cartaoCredito.Bandeira,
                Numero = CriptografiaHelper.Encriptar(cartaoCredito.Numero),
                NomeTitular = CriptografiaHelper.Encriptar(cartaoCredito.NomeTitular),
                DataValidade = CriptografiaHelper.Encriptar(cartaoCredito.DataValidade),
                CodigoSeguranca = CriptografiaHelper.Encriptar(cartaoCredito.CodigoSeguranca)
            };
        }
    }
}