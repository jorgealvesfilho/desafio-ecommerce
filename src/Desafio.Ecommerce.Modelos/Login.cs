﻿using System.ComponentModel.DataAnnotations;

namespace Desafio.Ecommerce.Modelos
{
    public class Login
    {
        [Required]
        public string Usuario { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Senha { get; set; }
    }
}