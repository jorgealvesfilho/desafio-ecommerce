﻿using Desafio.Ecommerce.Modelos;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;

namespace Desafio.Ecommerce.Persistencia
{
    public class TaxaAdquirenteRepositorio
    {
        private readonly IMongoCollection<TaxaAdquirente> taxasAdquirente;

        public TaxaAdquirenteRepositorio(IConfiguration config)
        {
            var client = new MongoClient(config.GetSection("MongoConnection:ConnectionString").Value);
            var database = client.GetDatabase(config.GetSection("MongoConnection:Database").Value);
            taxasAdquirente = database.GetCollection<TaxaAdquirente>("TaxasAdquirente");
        }

        public List<TaxaAdquirente> Recuperar()
        {
            return taxasAdquirente.Find(x => true).ToList();
        }

        public TaxaAdquirente Recuperar(string id)
        {
            ObjectId docId;
            try
            {
                docId = new ObjectId(id);
            }
            catch (Exception)
            {
                return null;
            }

            return taxasAdquirente.Find<TaxaAdquirente>(x => x.Id == docId).FirstOrDefault();
        }

        public TaxaAdquirente Recuperar(string adquirente, string bandeira)
        {
            return taxasAdquirente.Find<TaxaAdquirente>(x => x.Adquirente == adquirente && x.Bandeira == bandeira).FirstOrDefault();
        }

        public TaxaAdquirente Incluir(TaxaAdquirente pagamento)
        {
            taxasAdquirente.InsertOne(pagamento);
            return pagamento;
        }

        public List<TaxaAdquirente> Incluir(List<TaxaAdquirente> listaTaxasAdquirente)
        {
            taxasAdquirente.InsertMany(listaTaxasAdquirente);
            return listaTaxasAdquirente;
        }

        public void Alterar(string id, TaxaAdquirente taxaAdquirente)
        {
            ObjectId docId = new ObjectId(id);

            taxasAdquirente.ReplaceOne(x => x.Id == docId, taxaAdquirente);
        }
    }
}