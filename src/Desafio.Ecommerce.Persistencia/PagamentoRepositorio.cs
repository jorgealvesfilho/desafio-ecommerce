﻿using Desafio.Ecommerce.Modelos;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Desafio.Ecommerce.Persistencia
{
    public class PagamentoRepositorio
    {
        private readonly IMongoCollection<PagamentoProtegido> pagamentos;

        public PagamentoRepositorio(IConfiguration config)
        {
            var client = new MongoClient(config.GetSection("MongoConnection:ConnectionString").Value);
            var database = client.GetDatabase(config.GetSection("MongoConnection:Database").Value);
            pagamentos = database.GetCollection<PagamentoProtegido>("Pagamentos");
        }

        public List<PagamentoProtegido> Recuperar()
        {
            List<PagamentoProtegido> listaPagamentos = pagamentos.Find(x => true).ToList();
            return listaPagamentos;
        }

        public PagamentoProtegido Recuperar(string id)
        {
            ObjectId docId;
            try
            {
                docId = new ObjectId(id);
            }
            catch (Exception)
            {
                return null;
            }

            PagamentoProtegido pagamento = pagamentos.Find<PagamentoProtegido>(x => x.Id == docId).FirstOrDefault();
            return pagamento;
        }

        public PagamentoProtegido Incluir(PagamentoProtegido pagamento)
        {            
            pagamentos.InsertOne(pagamento);
            return pagamento;
        }

        public List<PagamentoProtegido> Incluir(List<PagamentoProtegido> listaPagamentos)
        {
            pagamentos.InsertMany(listaPagamentos);
            return listaPagamentos;
        }
    }
}