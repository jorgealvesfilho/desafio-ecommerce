﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace Desafio.Ecommerce.Seguranca
{
    public class SigningConfigurations
    {
        private readonly string secret = "testedeautenticacaodesafioecommerce";
        public SecurityKey Key { get; }
        public SigningCredentials SigningCredentials { get; }

        public SigningConfigurations()
        {
            byte[] keyByteArray = Encoding.ASCII.GetBytes(secret);
            Key = new SymmetricSecurityKey(keyByteArray);
            SigningCredentials = new SigningCredentials(Key, SecurityAlgorithms.HmacSha256);
        }
    }
}