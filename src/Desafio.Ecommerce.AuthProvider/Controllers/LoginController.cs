﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Desafio.Ecommerce.Seguranca;
using System.Threading.Tasks;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;
using IdentityUser = Microsoft.AspNetCore.Identity.MongoDB.IdentityUser;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Desafio.Ecommerce.Modelos;

namespace Desafio.Ecommerce.AuthProvider.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly UserManager<IdentityUser> userManager;
        private readonly SignInManager<IdentityUser> signInManager;
        private readonly ILogger<LoginController> logger;

        public LoginController(UserManager<IdentityUser> userManager,
                               SignInManager<IdentityUser> signInManager,
                               ILogger<LoginController> logger)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.logger = logger;
        }

        [HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Token([FromBody] Login login,
                                               [FromServices] SigningConfigurations signingConfigurations,
                                               [FromServices] TokenConfigurations tokenConfigurations)
        {
            if (!ModelState.IsValid)
            {
                logger.LogWarning("Dados de usuario invalidos!");
                return BadRequest();
            }

            logger.LogInformation("Recebendo dados de usuario para login e obtencao de token...");
            IdentityUser usuario = userManager.FindByNameAsync(login.Usuario).Result;

            if (usuario == null)
            {
                logger.LogWarning("Usuario nao registrado!");
                return Unauthorized();
            }

            logger.LogInformation("Verificando se o usuario esta autenticado na aplicacao...");
            SignInResult resultado = await signInManager.PasswordSignInAsync(usuario, login.Senha, true, true);

            if (!resultado.Succeeded)
            {
                logger.LogWarning("Usuario nao registrado!");
                return Unauthorized();
            }

            logger.LogInformation("Gerando o token para o usuario...");
            //cria token (header + payload >> direitos + signature)
            List<Claim> direitos = new List<Claim>();

            direitos.Add(new Claim(JwtRegisteredClaimNames.Sub, login.Usuario));
            direitos.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));

            logger.LogInformation("Incluindo no token perfis caso o usuario tenha...");
            foreach (string role in usuario.Roles)
            {
                direitos.Add(new Claim(ClaimTypes.Role, role));
            }

            JwtSecurityToken token = new JwtSecurityToken(
                issuer: tokenConfigurations.Issuer,
                audience: tokenConfigurations.Audience,
                claims: direitos,
                signingCredentials: signingConfigurations.SigningCredentials,
                expires: DateTime.Now.AddSeconds(tokenConfigurations.Seconds)
            );

            string tokenString = new JwtSecurityTokenHandler().WriteToken(token);

            logger.LogInformation("Token gerado com sucesso!");
            return Ok(tokenString);
        }

        [HttpPost("registro")]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Registro(Registro registro)
        {
            if (!ModelState.IsValid)
            {
                logger.LogWarning("Dados de registro de usuario invalidos!");
                return BadRequest();
            }

            logger.LogInformation("Recebendo dados de registro de usuario...");
            IdentityUser usuario = new IdentityUser();
            usuario.UserName = registro.Usuario;

            logger.LogInformation("Verificando se o usuario possui perfis associados...");
            if (registro.Perfis != null && registro.Perfis.Count > 0)
            {
                usuario.Roles = registro.Perfis;
            }

            logger.LogInformation("Criando o usuario...");
            IdentityResult resultadoUsuario = await userManager.CreateAsync(usuario, registro.Senha);

            if (!resultadoUsuario.Succeeded)
            {
                List<IdentityError> erros = (List<IdentityError>)resultadoUsuario.Errors;
                IdentityError erro = erros[0];

                logger.LogWarning("Criacao de usuario falhou: " + erro.Code + " - " + erro.Description);
                return BadRequest();
            }

            await signInManager.SignInAsync(usuario, true);

            logger.LogInformation("Usuario criado com sucesso!");
            return Ok();
        }
    }
}