﻿using Desafio.Ecommerce.Modelos;
using Desafio.Ecommerce.Persistencia;
using System.Collections.Generic;

namespace Desafio.Ecommerce.Servicos
{
    public class TaxaAdquirenteService
    {
        private readonly TaxaAdquirenteRepositorio repositorio;

        public TaxaAdquirenteService(TaxaAdquirenteRepositorio repositorio)
        {
                this.repositorio = repositorio;
        }

        public List<TaxaAdquirente> Recuperar()
        {
            return repositorio.Recuperar();
        }

        public TaxaAdquirente Recuperar(string id)
        {
            return repositorio.Recuperar(id);
        }

        public TaxaAdquirente Recuperar(string adquirente, string bandeira)
        {
            return repositorio.Recuperar(adquirente, bandeira);
        }

        public TaxaAdquirente Incluir(TaxaAdquirente pagamento)
        {
            return repositorio.Incluir(pagamento);
        }

        public List<TaxaAdquirente> Incluir(List<TaxaAdquirente> listaTaxasAdquirente)
        {
            return repositorio.Incluir(listaTaxasAdquirente);
        }

        public void Alterar(string id, TaxaAdquirente taxaAdquirente)
        {
            repositorio.Alterar(id, taxaAdquirente);
        }

        public bool VerificarExistenciaTaxaAdquirente(TaxaAdquirente taxaAdquirente)
        {
            TaxaAdquirente taxa = repositorio.Recuperar(taxaAdquirente.Adquirente,
                                                        taxaAdquirente.Bandeira);
            return taxa != null;
        }

        public bool VerificarExistenciaTaxaAdquirente(List<TaxaAdquirente> listaTaxasAdquirente)
        {
            foreach (TaxaAdquirente taxaAdquirente in listaTaxasAdquirente)
            {
                if (VerificarExistenciaTaxaAdquirente(taxaAdquirente))
                {
                    return true;
                }
            }
            return false;
        }
    }
}