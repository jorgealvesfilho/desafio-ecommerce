﻿using Desafio.Ecommerce.Modelos;
using Desafio.Ecommerce.Persistencia;
using System;
using System.Collections.Generic;

namespace Desafio.Ecommerce.Servicos
{
    public class PagamentoService : IPagamentoService
    {
        private readonly PagamentoRepositorio repositorio;
        private readonly TaxaAdquirenteService taxaAdquirenteService;

        public PagamentoService(PagamentoRepositorio repositorio,
                                TaxaAdquirenteService taxaAdquirenteService)
        {
            this.repositorio = repositorio;
            this.taxaAdquirenteService = taxaAdquirenteService;
        }

        public List<PagamentoProtegido> Recuperar()
        {
            return repositorio.Recuperar();
        }

        public PagamentoProtegido Recuperar(string id)
        {
            return repositorio.Recuperar(id);
        }

        public PagamentoProtegido Incluir(PagamentoProtegido pagamento)
        {
            ObterTaxaECalcularValoresAdquirenteELojista(pagamento);
            return repositorio.Incluir(pagamento);
        }

        public List<PagamentoProtegido> Incluir(List<PagamentoProtegido> listaPagamentos)
        {
            ObterTaxaECalcularValoresAdquirenteELojista(listaPagamentos);
            return repositorio.Incluir(listaPagamentos);
        }

        public void Alterar(PagamentoProtegido pagamento)
        {
            throw new NotImplementedException();
        }

        public void Excluir(PagamentoProtegido pagamento)
        {
            throw new NotImplementedException();
        }

        private void ObterTaxaECalcularValoresAdquirenteELojista(PagamentoProtegido pagamento)
        {
            TaxaAdquirente taxaAdquirente = taxaAdquirenteService.Recuperar(pagamento.Adquirente, pagamento.CartaoCredito.Bandeira);

            if (taxaAdquirente == null)
            {
                pagamento.TaxaAdquirente = decimal.Zero;
            }
            else
            {
                pagamento.TaxaAdquirente = taxaAdquirente.Valor;
            }

            pagamento.ValorAdquirente = Math.Round((pagamento.Valor * pagamento.TaxaAdquirente) / 100m, 2, MidpointRounding.ToEven);
            pagamento.ValorLojista = pagamento.Valor - pagamento.ValorAdquirente;
        }

        private void ObterTaxaECalcularValoresAdquirenteELojista(List<PagamentoProtegido> listaPagamentos)
        {
            foreach (PagamentoProtegido pagamento in listaPagamentos)
            {
                ObterTaxaECalcularValoresAdquirenteELojista(pagamento); ;
            }
        }
    }
}
