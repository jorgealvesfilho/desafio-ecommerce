﻿using Desafio.Ecommerce.Modelos;
using System.Collections.Generic;

namespace Desafio.Ecommerce.Servicos
{
    public interface IPagamentoService
    {
        List<PagamentoProtegido> Recuperar();
        PagamentoProtegido Recuperar(string id);
        PagamentoProtegido Incluir(PagamentoProtegido pagamento);
        List<PagamentoProtegido> Incluir(List<PagamentoProtegido> listaPagamentos);
        void Alterar(PagamentoProtegido pagamento);
        void Excluir(PagamentoProtegido pagamento);
    }
}