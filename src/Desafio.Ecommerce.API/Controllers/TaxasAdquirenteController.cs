﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Desafio.Ecommerce.Modelos;
using Microsoft.Extensions.Logging;
using Desafio.Ecommerce.Servicos;

namespace Desafio.Ecommerce.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TaxasAdquirenteController : ControllerBase
    {
        private readonly TaxaAdquirenteService taxaAdquirenteService;
        private readonly ILogger<TaxasAdquirenteController> logger;

        public TaxasAdquirenteController(TaxaAdquirenteService taxaAdquirenteService,
                                         ILogger<TaxasAdquirenteController> logger)
        {
            this.taxaAdquirenteService = taxaAdquirenteService;
            this.logger = logger;
        }

        // GET api/taxasadquirente
        [HttpGet]
        public ActionResult<IEnumerable<TaxaAdquirente>> Listar()
        {
            logger.LogInformation("Recuperando lista de taxas de adquirente...");
            return taxaAdquirenteService.Recuperar();
        }

        // GET api/taxasadquirente/{id}
        [HttpGet("{id}")]
        public ActionResult<TaxaAdquirente> Recuperar(string id)
        {
            logger.LogInformation("Recuperando taxa de adquirente...");
            TaxaAdquirente taxaAdquirente = taxaAdquirenteService.Recuperar(id);

            if (taxaAdquirente == null)
            {
                logger.LogWarning("Taxa de adquirente nao encontrada!");
                return NotFound();
            }

            return taxaAdquirente;
        }

        // POST api/taxasadquirente
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public IActionResult Incluir([FromBody] TaxaAdquirente taxaAdquirente)
        {
            if (!ModelState.IsValid)
            {
                logger.LogWarning("Dados de taxa de adquirente invalidos!");
                return BadRequest(ModelState);
            }

            if (taxaAdquirenteService.VerificarExistenciaTaxaAdquirente(taxaAdquirente))
            {
                logger.LogWarning("Taxa de adquirente ja cadastrada!");
                return BadRequest();
            }

            logger.LogInformation("Incluindo taxa de adquirente...");
            taxaAdquirenteService.Incluir(taxaAdquirente);

            logger.LogInformation("Taxa de adquirente incluido com sucesso!");
            return Created("api/taxasadquirente/" + taxaAdquirente.Id.ToString(), taxaAdquirente);
        }

        // POST api/taxasadquirente/lista
        [Authorize(Roles = "Administrador")]
        [HttpPost("lista")]
        public IActionResult IncluirLista([FromBody] List<TaxaAdquirente> listaTaxasAdquirente)
        {
            if (!ModelState.IsValid)
            {
                logger.LogWarning("Alguma taxa de adquirente com dados invalidos!");
                return BadRequest(ModelState);
            }

            if (taxaAdquirenteService.VerificarExistenciaTaxaAdquirente(listaTaxasAdquirente))
            {
                logger.LogWarning("Alguma das taxas de adquirente ja cadastrada!");
                return BadRequest();
            }

            logger.LogInformation("Incluindo lista de taxas de adquirente...");
            taxaAdquirenteService.Incluir(listaTaxasAdquirente);

            logger.LogInformation("Lista de taxas de adquirente incluidas com sucesso!");
            return Ok(listaTaxasAdquirente);
        }

        // PUT api/taxasadquirente
        [Authorize(Roles = "Administrador")]
        [HttpPut]
        public IActionResult Alterar([FromBody] TaxaAdquirente taxaAdquirente)
        {
            if (!ModelState.IsValid)
            {
                logger.LogWarning("Dados de taxa de adquirente invalidos!");
                return BadRequest(ModelState);
            }

            logger.LogInformation("Recuperando taxa de adquirente...");
            TaxaAdquirente taxa = taxaAdquirenteService.Recuperar(taxaAdquirente.Adquirente,
                                                                    taxaAdquirente.Bandeira);
            if (taxa == null)
            {
                logger.LogWarning("Taxa de adquirente nao encontrada!");
                return NotFound();
            }

            taxaAdquirente.Id = taxa.Id;

            logger.LogInformation("Alterando taxa de adquirente...");
            taxaAdquirenteService.Alterar(taxa.Id.ToString(), taxaAdquirente);

            logger.LogInformation("Taxa de adquirente alterada com sucesso!");
            return Ok(taxaAdquirente);
        }
    }
}