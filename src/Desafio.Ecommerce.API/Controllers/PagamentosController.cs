﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Desafio.Ecommerce.Modelos;
using Microsoft.Extensions.Logging;
using Desafio.Ecommerce.Servicos;

namespace Desafio.Ecommerce.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PagamentosController : ControllerBase
    {
        private readonly IPagamentoService pagamentoService;
        private readonly ILogger<PagamentosController> logger;

        public PagamentosController(IPagamentoService pagamentoService,
                                    ILogger<PagamentosController> logger)
        {
            this.pagamentoService = pagamentoService;
            this.logger = logger;
        }

        // GET api/pagamentos
        [HttpGet]
        public ActionResult<IEnumerable<PagamentoSaida>> Recuperar()
        {
            logger.LogInformation("Recuperando lista de pagamentos...");
            return Ok(pagamentoService.Recuperar().ConvertAll(x => x.ToSaida()));

        }

        // GET api/pagamentos/{id}
        [HttpGet("{id}")]
        public ActionResult<PagamentoSaida> Recuperar(string id)
        {
            logger.LogInformation("Recuperando pagamento...");
            PagamentoProtegido pagamento = pagamentoService.Recuperar(id);

            if (pagamento == null)
            {
                logger.LogWarning("Pagamento nao encontrado!");
                return NotFound();
            }

            return pagamento.ToSaida();
        }

        // POST api/pagamentos
        [HttpPost]
        public IActionResult Incluir([FromBody] Pagamento pagamento)
        {
            if (!ModelState.IsValid)
            {
                logger.LogWarning("Dados de pagamento invalidos!");
                return BadRequest(ModelState);
            }

            PagamentoProtegido pagamentoProtegido = pagamento.ToPagamentoProtegido();

            logger.LogInformation("Incluindo pagamento...");
            pagamentoService.Incluir(pagamentoProtegido);

            logger.LogInformation("Pagamento incluido com sucesso!");
            return Created("api/pagamentos/" + pagamento.Id.ToString(), pagamentoProtegido.ToSaida());
        }

        // POST api/pagamentos/lista
        [HttpPost("lista")]
        public IActionResult IncluirLista([FromBody] List<Pagamento> listaPagamentos)
        {
            if (!ModelState.IsValid)
            {
                logger.LogWarning("Algum pagamento com dados invalidos!");
                return BadRequest(ModelState);
            }

            List<PagamentoProtegido> listaPagamentosProtegido = listaPagamentos.ConvertAll(x => x.ToPagamentoProtegido());

            logger.LogInformation("Incluindo lista de pagamentos...");
            pagamentoService.Incluir(listaPagamentosProtegido);

            logger.LogInformation("Lista de pagamento incluidos com sucesso!");
            return Ok(listaPagamentosProtegido.ConvertAll(x => x.ToSaida()));
        }
    }
}