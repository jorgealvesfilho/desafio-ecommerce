﻿using Desafio.Ecommerce.API.Controllers;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace Desafio.Ecommerce.Teste
{
    public class LoggerFake : ILogger<PagamentosController>
    {
        public static Exception ProvidedException { get; set; }
        public static string ProvidedMessage { get; set; }
        public static object[] ProvidedArgs { get; set; }
        public IDisposable BeginScope<TState>(TState state)
        {
            return null;
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return true;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
        }

        public void LogError(Exception ex, string message, params object[] args)
        {
            ProvidedException = ex;
            ProvidedMessage = message;
            ProvidedArgs = args;
        }
    }
}
