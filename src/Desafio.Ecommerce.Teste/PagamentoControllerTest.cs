﻿using Desafio.Ecommerce.API.Controllers;
using Desafio.Ecommerce.Modelos;
using Desafio.Ecommerce.Servicos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using Xunit;

namespace Desafio.Ecommerce.Teste
{
    public class PagamentoControllerTest
    {
        private IPagamentoService pagamentoService;
        private ILogger<PagamentosController> logger;
        private PagamentosController pagamentosController;

        public PagamentoControllerTest()
        {
            //var serviceProvider = new ServiceCollection()
            //              //.AddLogging()
            //              .BuildServiceProvider();
            //var factory = serviceProvider.GetService<ILoggerFactory>();
            //var logger = factory.CreateLogger<PagamentosController>();
            logger = new LoggerFake();
            pagamentoService = new PagamentoServiceFake();
            pagamentosController = new PagamentosController(pagamentoService, logger);
        }

        [Fact]
        public void Recuperar_WhenCalled_ReturnsOkResult()
        {
            // Act
            var okResult = pagamentosController.Recuperar();

            // Assert
            Assert.IsType<OkObjectResult>(okResult.Result);
        }

        [Fact]
        public void Recuperar_WhenCalled_ReturnsAllItems()
        {
            // Act
            var okResult = pagamentosController.Recuperar().Result as OkObjectResult;

            // Assert
            var items = Assert.IsType<List<PagamentoSaida>>(okResult.Value);
            Assert.Single(items);
        }

        [Fact]
        public void Incluir_InvalidObjectPassed_ReturnsBadRequest()
        {
            // Arrange
            Pagamento adquirenteVazioPagamento = new Pagamento()
            {
                Adquirente = "",
                Valor = 1000,
                CartaoCredito = new CartaoCredito()
                {
                    Bandeira = "Master",
                    Numero = "1234123412341234",
                    NomeTitular = "Marcos Penteado",
                    DataValidade = "12/20",
                    CodigoSeguranca = "123"
                }
            };
            pagamentosController.ModelState.AddModelError("Adquirente", "Required");

            // Act
            var badResponse = pagamentosController.Incluir(adquirenteVazioPagamento);

            // Assert
            Assert.IsType<BadRequestObjectResult>(badResponse);
        }


        [Fact]
        public void Incluir_ValidObjectPassed_ReturnsCreatedResponse()
        {
            // Arrange
            Pagamento pagamento = new Pagamento()
            {
                Adquirente = "Cielo",
                Valor = 1000,
                CartaoCredito = new CartaoCredito()
                {
                    Bandeira = "Master",
                    Numero = "1234123412341234",
                    NomeTitular = "Marcos Penteado",
                    DataValidade = "12/20",
                    CodigoSeguranca = "123"
                }
            };

            // Act
            var createdResponse = pagamentosController.Incluir(pagamento);

            // Assert
            Assert.IsType<CreatedResult>(createdResponse);
        }


        [Fact]
        public void Incluir_ValidObjectPassed_ReturnedResponseHasCreatedItem()
        {
            // Arrange
            Pagamento pagamento = new Pagamento()
            {
                Adquirente = "Cielo",
                Valor = 1000,
                CartaoCredito = new CartaoCredito()
                {
                    Bandeira = "Master",
                    Numero = "1234123412341234",
                    NomeTitular = "Marcos Penteado",
                    DataValidade = "12/20",
                    CodigoSeguranca = "123"
                }
            };

            // Act
            var createdResponse = pagamentosController.Incluir(pagamento) as CreatedResult;
            PagamentoSaida item = createdResponse.Value as PagamentoSaida;

            // Assert
            Assert.IsType<PagamentoSaida>(item);
            Assert.Equal("Cielo", item.Adquirente);
        }
    }
}
