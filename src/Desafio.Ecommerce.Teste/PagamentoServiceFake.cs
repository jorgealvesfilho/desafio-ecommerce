using Desafio.Ecommerce.Modelos;
using Desafio.Ecommerce.Servicos;
using MongoDB.Bson;
using System;
using System.Collections.Generic;

namespace Desafio.Ecommerce.Teste
{
    public class PagamentoServiceFake : IPagamentoService
    {
        private readonly List<PagamentoProtegido> pagamentos;

        public PagamentoServiceFake()
        {
            pagamentos = new List<PagamentoProtegido>()
            {
                new Pagamento() {
                    Adquirente = "Cielo",
                    Valor = 1000,
                    CartaoCredito = new CartaoCredito() {
                                        Bandeira ="Visa",
                                        Numero = "1234123412341234",
                                        NomeTitular = "Marcos Penteado",
                                        DataValidade = "12/20",
                                        CodigoSeguranca = "123"
                    }
                }.ToPagamentoProtegido()
            };
        }

        public List<PagamentoProtegido> Recuperar()
        {
            return pagamentos;
        }

        public PagamentoProtegido Recuperar(string id)
        {
            ObjectId docId;
            try
            {
                docId = new ObjectId(id);
            }
            catch (Exception)
            {
                return null;
            }

            PagamentoProtegido pagamento = pagamentos.Find(x => x.Id == docId);
            return pagamento;
        }

        public PagamentoProtegido Incluir(PagamentoProtegido pagamento)
        {
            pagamentos.Add(pagamento);
            return pagamento;
        }

        public List<PagamentoProtegido> Incluir(List<PagamentoProtegido> listaPagamentos)
        {
            foreach (PagamentoProtegido pagamento in listaPagamentos)
            {
                pagamentos.Add(pagamento);
            }

            return listaPagamentos;
        }

        public void Alterar(PagamentoProtegido pagamento)
        {
            throw new NotImplementedException();
        }

        public void Excluir(PagamentoProtegido pagamento)
        {
            throw new NotImplementedException();
        }
    }
}